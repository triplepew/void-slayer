﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class FriendControl : MonoBehaviour
{
	public float maxSpeed = 25.0f;
	public float fleeDist = 1.0f;
	public float seekDist = 3.0f;


	[HideInInspector] public static FriendControl control;
	[HideInInspector] public PlayMakerFSM aiFSM;
	[HideInInspector] public GameObject target;

	void Awake ()
	{
		PlayMakerFSM[] temp = GetComponents<PlayMakerFSM>();
		foreach (PlayMakerFSM fsm in temp)
		{
			if (fsm.FsmName == "Damage Listener")
			{
				aiFSM = fsm;
				break;
			}
		}
	}

	void Start ()
	{
		target = GameObject.FindGameObjectWithTag ("Player");
	}
}
