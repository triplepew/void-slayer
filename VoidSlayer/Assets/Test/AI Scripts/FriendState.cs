﻿using UnityEngine;
using System.Collections;

public interface FriendState
{
	/// <summary>
	/// Updates the state.
	/// </summary>
	void UpdateState();

	/// <summary>
	/// Transitions to seek state
	/// </summary>
	void ToSeekState();
}
