﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class FriendFlee : MonoBehaviour 
{
	private Vector2 desiredVel;
	private Vector2 currentVel;
	private Vector2 steerVel;
	private float arcSharpness = 1;
	
	void Update () 
	{
		// Get the current velocity of the agent
		currentVel = GetComponent<Rigidbody2D>().velocity;
		
		//Calculate desired velocity vector
		desiredVel = new Vector2 (transform.position.x 
		                          - FriendControl.control.target.transform.position.x,
		                          transform.position.y 
		                          - FriendControl.control.target.transform.position.y);
		//desiredVel.Normalize();
		desiredVel = desiredVel * FriendControl.control.maxSpeed;
		
		//Use desired velocity to calculate steering force
		steerVel = desiredVel - currentVel;
		steerVel = steerVel * arcSharpness;  //should create larger arc
		
		//Apply steering force to agent
		GetComponent<Rigidbody2D>().AddForce (steerVel, ForceMode2D.Force);
		
		//Set rotation of agent
		float angle = Mathf.Atan2 (currentVel.x, currentVel.y) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);	
		
		if (FriendControl.control.seekDist <= Vector2.Distance (FriendControl.control.target.transform.position,
		                                                        transform.position))
		{
			FriendControl.control.aiFSM.ChangeState(FsmEvent.GetFsmEvent("Seek"));
		}
	}	
}
