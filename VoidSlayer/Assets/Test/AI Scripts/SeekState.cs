﻿using UnityEngine;
using System.Collections;

public class SeekState : FriendState 
{
	private readonly FriendStatePattern friend;

	private Vector2 desiredVel;
	private Vector2 currentVel;
	private Vector2 steerVel;

	public SeekState (FriendStatePattern friendStatePattern)
	{
		friend = friendStatePattern;
	}
	public void UpdateState()
	{
		friend.GetComponent<SpriteRenderer>().color = Color.green;
		Seek ();
	}

	public void ToSeekState()
	{
		Debug.Log ("Can not transition to same AIState");
	}

	private void Seek ()
	{
		// Get the current velocity of the agent
		currentVel = friend.GetComponent<Rigidbody2D>().velocity;
		
		//Calculate desired velocity vector
		desiredVel = new Vector2 (friend.target.transform.position.x 
		                          - friend.transform.position.x,
		                          friend.target.transform.position.y 
		                          - friend.transform.position.y);
		//desiredVel.Normalize();
		desiredVel = desiredVel * friend.maxSpeed;
		
		//Use desired velocity to calculate steering force
		steerVel = desiredVel - currentVel;
		
		//Apply steering force to agent
		friend.GetComponent<Rigidbody2D>().AddForce (steerVel, ForceMode2D.Force);
		
		//Set rotation of agent
		float angle = Mathf.Atan2 (currentVel.x, currentVel.y) * Mathf.Rad2Deg;
		friend.transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);	
	}
}
