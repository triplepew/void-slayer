﻿using UnityEngine;
using System.Collections;

public class FriendStatePattern : MonoBehaviour 
{
	[HideInInspector] public GameObject target;
	[HideInInspector] public GameObject agent;

	public float maxSpeed = 15.0f;
	
	//  				STATE OBJECTS
	//---------------------------------------------------
	[HideInInspector] public FriendState currentState;
	[HideInInspector] public SeekState seekState;

	void Awake ()
	{
		seekState = new SeekState (this);
	}

	void Start ()
	{
		currentState = seekState;
		target = GameObject.FindGameObjectWithTag ("Player");
		agent = gameObject;
	}

	void Update ()
	{
		currentState.UpdateState();
	}
}
