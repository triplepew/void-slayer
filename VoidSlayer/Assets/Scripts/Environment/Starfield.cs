﻿using UnityEngine;
using System.Collections;

public class Starfield : MonoBehaviour 
{
	private Transform trans;
	private ParticleSystem.Particle[] stars;

	public int starsMax = 100;
	public float starSize = 1;
	public float starDist = 10;
	public float starClipDist = 5;
	private float starDistSqr;
	private float starClipDistSqr;

	void Start ()
	{
		trans = transform;
		starDistSqr = starDist * starDist;
		starClipDistSqr = starClipDist * starClipDist;
	}

	private void CreateField()
	{
		//Create a new particle system
		stars = new ParticleSystem.Particle[starsMax];

		//Create stars in random locations within set bounds
		for (int i = 0; i < starsMax; i++) 
		{
			stars[i].position = Random.insideUnitSphere * starDist + trans.position;
			stars[i].color = new Color(1,1,1,1);
			stars[i].size = starSize;
		}
	}

	void Update ()
	{
		//if no field exists
		if (stars == null) CreateField ();

		//check each stars position
		for (int i = 0; i < starsMax; i++) 
		{
			//if too far respawn
			if((stars[i].position - trans.position).sqrMagnitude > starDistSqr)
			{
				stars[i].position = Random.insideUnitSphere.normalized *starDist + trans.position;
			}

			//if too close fade by percentage
			if((stars[i].position - trans.position).sqrMagnitude <= starClipDistSqr)
			{
				float percent = (stars[i].position - trans.position).sqrMagnitude / starClipDistSqr;
				stars[i].color = new Color(1,1,1, percent);
				stars[i].size = percent * starSize;
			}
		}

		GetComponent<ParticleSystem>().SetParticles (stars, stars.Length);
	}
}
