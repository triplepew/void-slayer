﻿using UnityEngine;
using System.Collections;

public class OnPause : MonoBehaviour 
{
	public GameObject pauseCanvas;

	void Update () 
	{
		if (GameControl.control.isPaused)
		{
			pauseCanvas.SetActive(true);
		}
		else pauseCanvas.SetActive(false);
	}
}
