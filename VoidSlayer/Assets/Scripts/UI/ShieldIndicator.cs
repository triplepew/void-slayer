﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using HutongGames.PlayMaker;

public class ShieldIndicator : MonoBehaviour 
{
    public Sprite shield_blue;
    public Sprite shield_yellow;

    private Image displayImage;
    private GameObject player;
    private float player_shield;
    private float start_shield;

	// Use this for initialization
	void Start () 
    {
        //Get reference to Image Object
        displayImage = GetComponent<Image>();

        //Get starting shield amount
        start_shield = FsmVariables.GlobalVariables.GetFsmInt("Start Shield").Value;

        Debug.Log(player_shield);
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Get current value of shield
        player_shield = FsmVariables.GlobalVariables.GetFsmInt("Player Shields").Value;

        if (start_shield != 0)
        {
            //Update fill amount
            displayImage.fillAmount = player_shield / start_shield;
        }

        //shield level is at 25% or lower
        if (displayImage.fillAmount < 0.25f)
        {
            displayImage.sprite = shield_yellow;
        }//end if

        else displayImage.sprite = shield_blue;
    }//end Update()
}




