﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Settings : MonoBehaviour 
{
	public Toggle physicsToggle;

	void Start ()
	{
		physicsToggle.isOn = GameControl.control.physicsOn;
	}

	void Update ()
	{
		if (physicsToggle.isOn != GameControl.control.physicsOn)
		{
			GameControl.control.physicsOn = physicsToggle.isOn;
		}
	}

}
