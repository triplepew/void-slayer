﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using HutongGames.PlayMaker;

public class WarningTimer : MonoBehaviour 
{
	public Text timer;

	private GameObject player;
	private float previoustime;
	private float currentTime;

	void OnEnable ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		timer.text = "5";
		previoustime = Time.realtimeSinceStartup;
	}

	void Update()
	{
		currentTime = Time.realtimeSinceStartup - previoustime;

		if (currentTime < 2 && currentTime >= 1)
		{
			timer.text = "4";
		}
		else if (currentTime < 3 && currentTime >= 2)
		{
			timer.text = "3";
		}
		else if (currentTime < 4 && currentTime >= 3)
		{
			timer.text = "2";
		}
		else if (currentTime < 5 && currentTime >= 4)
		{
			timer.text = "1";
		}
		else if (currentTime >= 5)
		{
			FsmVariables.GlobalVariables.GetFsmBool("isPaused").Value = true;
			gameObject.SetActive (false);
		}
	}
}
