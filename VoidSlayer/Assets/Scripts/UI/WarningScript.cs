﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using HutongGames.PlayMaker;

public class WarningScript : MonoBehaviour 
{
	public GameObject warningCanvas;
	
	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Player")
		{
			warningCanvas.SetActive (true);
		}
	}
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player")
		{
			warningCanvas.SetActive (false);
		}
	}
}
