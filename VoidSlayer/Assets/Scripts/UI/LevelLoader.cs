﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using HutongGames.PlayMaker;

public class LevelLoader : MonoBehaviour 
{
	public GameObject progressBar;
	public GameObject levelLoader;

	private Image progressBarImage;
	private int levelIndex;
	private float loadProgress;

	public void LoadNextLevel( int level)
	{
		levelIndex = level;
		loadProgress = 0.0f;
		progressBarImage = progressBar.GetComponent<Image>();
		FsmVariables.GlobalVariables.GetFsmBool("isPaused").Value = false;

		StartCoroutine(DisplayLoadingScreen (levelIndex));
	}

	IEnumerator DisplayLoadingScreen (int level)
	{
		levelLoader.SetActive (true);
		progressBarImage.fillAmount = loadProgress;

		AsyncOperation async = Application.LoadLevelAsync (level);

		while (!async.isDone)
		{
			loadProgress = async.progress;
			progressBarImage.fillAmount = loadProgress;
			yield return null;
		}
	}
}



















