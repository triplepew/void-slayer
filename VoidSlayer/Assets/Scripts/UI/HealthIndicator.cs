﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using HutongGames.PlayMaker;

public class HealthIndicator : MonoBehaviour 
{
	public Sprite health_blue;
	public Sprite health_yellow;
	public Sprite health_red;
	
	private Image displayImage;
	private GameObject player;
	private float player_health;
	private float start_health;
	private float health_percent;

	void Start () 
	{
		//Get reference to Image Object
		displayImage = GetComponent<Image>();
		
		//Get starting shield amount
		start_health = FsmVariables.GlobalVariables.GetFsmInt("Start Health").Value;
	}

	void Update () 
	{
		//Get current value of shield
		player_health = FsmVariables.GlobalVariables.GetFsmInt("Player Health").Value;

		// Get health percentage
		health_percent = player_health / start_health;

		// Display Image according to percentage
		if (health_percent < 0.25f)
		{
			displayImage.sprite = health_red;
		}
		else if (health_percent < 0.5f && health_percent > 0.25f)
		{
			displayImage.sprite = health_yellow;
		}
		
		else displayImage.sprite = health_blue;
	}
}
