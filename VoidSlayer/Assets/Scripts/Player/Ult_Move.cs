﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class Ult_Move : MonoBehaviour 
{
	private Vector2 joystickPos;
    private float speed = 0.0f;
	private bool physicsOn;
	
	void Start ()
	{

		physicsOn = GameControl.control.physicsOn;
	}

	void FixedUpdate () 
    {
        //Get the speed, higher if boosting
        speed = FsmVariables.GlobalVariables.GetFsmFloat("Speed").Value;

        //Get joystick position
        joystickPos = UltimateJoystick.GetPosition("Movement");

        if (physicsOn)
		{
			PhysicsMove();
		}
		else TransformMove();
	}//end FixedUpdate()

	/// <summary>
	/// Moves the player using the transform
	/// This allows for quick movements by the player
	/// </summary>
	void TransformMove ()
	{		
		//Move the object
		transform.position = new Vector3 (transform.position.x + (joystickPos.x/speed),
		                                  transform.position.y + (joystickPos.y/speed),
		                                  transform.position.z);
		
		//Rotate the object
		if (joystickPos != Vector2.zero)
		{
			float angle = Mathf.Atan2(joystickPos.x, joystickPos.y) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler (0.0f, 0.0f, -angle);
		}//end if
	}

	/// <summary>
	/// Moves the player using the Rigidbody2D
	/// This makes the game hareder as movement
	/// is more delayed. Especially for turning around
	/// </summary>
	void PhysicsMove ()
	{
		//Move the object
		GetComponent<Rigidbody2D>().AddForce(joystickPos * speed);
		
		//Rotate the object
		Vector2 moveDir = GetComponent<Rigidbody2D>().velocity;
		if (joystickPos != Vector2.zero)
		{
			float angle = Mathf.Atan2(moveDir.x, moveDir.y) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
		}//end if
	}
}
