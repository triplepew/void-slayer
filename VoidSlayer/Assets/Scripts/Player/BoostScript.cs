﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class BoostScript : MonoBehaviour 
{
    public void boost_start()
    {
        FsmVariables.GlobalVariables.GetFsmFloat("Speed").Value = 5.0f;
    }
    
    public void boost_end()
    {
        FsmVariables.GlobalVariables.GetFsmFloat("Speed").Value = 10.0f;
    }

	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			FsmVariables.GlobalVariables.GetFsmFloat("Speed").Value = 5.0f;
		}
		if (Input.GetKeyUp (KeyCode.Space))
		{
			FsmVariables.GlobalVariables.GetFsmFloat("Speed").Value = 10.0f;
		}
	}
}
