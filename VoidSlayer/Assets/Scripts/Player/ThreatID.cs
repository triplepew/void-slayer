﻿using UnityEngine;
using System.Collections;

public class ThreatID : MonoBehaviour 
{
    public GameObject threat_id;
    private GameObject[] threat_array;			//Reference all enemies
    private GameObject[] threatid_array;		//Store all ID objects

    private float angle = 0f;
    private int index;
    private Vector2 threat_dir = Vector2.zero;
    private Vector2 direction = Vector2.zero;
	private Transform PlayerTrans;

    void Start()
    {
		//Get player Transform
		PlayerTrans = transform;
        //Get list of enemies, get its length
        threat_array = GameObject.FindGameObjectsWithTag("Enemy");

        if (threat_array != null)
        {
            ThreatSetup();
        }//end if
    }//end start

	private void ThreatSetup()
	{
		//creat new group of indicators
		index = threat_array.Length;
		threatid_array = new GameObject[index];
		
		//instantiate game objects
		//for each threat
		for (int i = 0; i < index; i++)
		{
			threatid_array[i] = (GameObject)Instantiate(
				threat_id,
				PlayerTrans.position,
				PlayerTrans.rotation);
		}
	}
	
	// Update is called once per frame
	void Update () 
    {
		//Make sure there are an equal number of threats
        if (index == threat_array.Length)
        {
            //for each threat
            for (int i = 0; i < index; i++)
            {
                //Must have try block
				//If enemy is termed inside loop
				//exception will be thrown
                try 
                { 
					//Update direction of threat
                    direction = PlayerTrans.position - threat_array[i].transform.position;
					//Update position to follow player
					threatid_array[i].transform.position = PlayerTrans.position;
                }

                catch 
                {
					//Recreate threat ID objects
                    for (int v = 0; v < threatid_array.Length; v++)
                    {
                        Destroy(threatid_array[v]);
                    }
                    ThreatSetup();
					break;
                }

                //normalize
                threat_dir = direction.normalized;
                threat_dir = new Vector2(-threat_dir.x, -threat_dir.y);

                //Get angle and apply to object rotation
                if (threat_dir != Vector2.zero)
                {
                    angle = Mathf.Atan2(threat_dir.x, threat_dir.y) * Mathf.Rad2Deg;
                    //threatid_array[i].transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
					threatid_array[i].transform.rotation = Quaternion.Euler (0.0f, 0.0f, -angle);

                }//end if

                //Update position to follow player
                threatid_array[i].transform.position = PlayerTrans.position;
            }//end for

            //Update number of threats
            threat_array = GameObject.FindGameObjectsWithTag("Enemy");

			//If an enemy has died
			if (index > threat_array.Length)
			{
				for (int i = 0; i < threatid_array.Length; i++)
				{
					//Remove existing threat_id objects
					Destroy(threatid_array[i]);
				}
				//create new group of ID objects
				ThreatSetup();
			}//end if
        }//end else
	}//end LateUpdate()
}
