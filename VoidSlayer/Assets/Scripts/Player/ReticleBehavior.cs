﻿using UnityEngine;
using System.Collections;

public class ReticleBehavior : MonoBehaviour 
{
	private GameObject player;
	private Vector2 joystickPos;
	private Transform trans;
	private float angle;

	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		trans = transform;
	}

	void Update ()
	{
		joystickPos = UltimateJoystick.GetPosition("Shoot");

		if (joystickPos != Vector2.zero)
		{
			angle = Mathf.Atan2(joystickPos.x, joystickPos.y) * Mathf.Rad2Deg;
			angle = angle - 90;
			trans.rotation = Quaternion.AngleAxis(angle, Vector3.back);
		}

		trans.position = new Vector3 (player.transform.position.x + (trans.right.x * 0.8f),
		                              player.transform.position.y + (trans.right.y * 0.8f),
		                              0.0f);
	}
}
