﻿using UnityEngine;
using System.Collections;

public class ShieldAnim : MonoBehaviour 
{
	private Animator anim;
	private int shieldHash = Animator.StringToHash ("Shield Hit");
	
	void Start () 
	{
		anim = GetComponent<Animator>();
	}

	void AnimateShield () 
	{
		anim.SetTrigger (shieldHash);
	}
}
