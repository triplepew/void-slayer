﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class LoadPlayer : MonoBehaviour 
{
	void OnEnable ()
	{
		FsmVariables.GlobalVariables.GetFsmInt("Player Health").Value = FsmVariables.GlobalVariables.GetFsmInt("Start Health").Value;
		FsmVariables.GlobalVariables.GetFsmInt("Player Shields").Value = FsmVariables.GlobalVariables.GetFsmInt("Start Shield").Value;
	}
}
