﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class Ult_Shoot : MonoBehaviour 
{
    private int timer = 0;
    public GameObject laser;

	// Update is called once per frame
	void FixedUpdate () 
    {
        //Get joystick position
        Vector2 joystickPos = UltimateJoystick.GetPosition("Shoot");
        if (joystickPos != Vector2.zero)
        {
            //Rotate Laser Turret
            float angle = Mathf.Atan2(joystickPos.x, joystickPos.y) * Mathf.Rad2Deg;
            angle = angle - 90;
            //change rotation along z axis
            //according to calculated angle
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);

            //shoot
            if (timer == 0)
            {
                //Instantiate
                GameObject Projectile = (GameObject)Instantiate(
                    laser,
                    this.transform.position,
                    this.transform.rotation);
                timer = 10;
            }//end if
        }//end if
        if (timer > 0) --timer;
	}//end if
}//end FixedUpdate()
