﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class IndicatorGroup : MonoBehaviour 
{
	public GameObject agent;
	public GameObject shieldID;
	public GameObject overShieldID;
	public float shieldDivideLvl;

	private PlayMakerFSM damageLis;
	private float shieldLevel;

	void Awake ()
	{
		PlayMakerFSM[] temp = agent.GetComponents<PlayMakerFSM>();
		foreach (PlayMakerFSM fsm in temp)
		{
			if (fsm.FsmName == "Damage Listener")
			{
				damageLis = fsm;
				break;
			}
		}

		shieldLevel = damageLis.FsmVariables.GetFsmInt ("Shield").Value;

		if (shieldLevel == 0 || shieldLevel == null)
		{
			shieldID.SetActive(false);
		}
		if (shieldLevel <= shieldDivideLvl)
		{
			overShieldID.SetActive(false);
		}
	}

	void Update ()
	{
		transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
	}
}
