﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class ShieldID : MonoBehaviour 
{
	public GameObject agent;
	public float shieldDivideLvl;

	private PlayMakerFSM damageLis;
	private Vector3 shieldScale;
	private float startShield;
	private float currentShield;
	private float shieldDiff;
	private float startShieldScale;
	private float newScale;

	void Awake ()
	{
		PlayMakerFSM[] temp = agent.GetComponents<PlayMakerFSM>();
		foreach (PlayMakerFSM fsm in temp)
		{
			if (fsm.FsmName == "Damage Listener")
			{
				damageLis = fsm;
				break;
			}
		}
	}

	void Start ()
	{
		startShield = damageLis.FsmVariables.GetFsmInt("Shield").Value;

		if (startShield > shieldDivideLvl)
		{
			startShield = startShield / 2;
		}

		shieldScale = transform.localScale;
		startShieldScale = shieldScale.x;

		shieldDiff = 1.0f;
	}

	void Update ()
	{
		currentShield = damageLis.FsmVariables.GetFsmInt ("Shield").Value;

		if (currentShield < startShield)
		{
			shieldDiff = currentShield / startShield;
			newScale = startShieldScale * shieldDiff;
			transform.localScale = new Vector3 (newScale, shieldScale.y, shieldScale.z);
		}
		else transform.localScale = new Vector3 (shieldScale.x, shieldScale.y, shieldScale.z);
	}

	void LateUpdate ()
	{
		if (shieldDiff > 0.5f)
		{
			GetComponent<SpriteRenderer>().color = new Color (200, 120, 255);
		}
		else if (shieldDiff > 0.25)
		{
			GetComponent<SpriteRenderer>().color = new Color (255, 255, 0);
		}
		else GetComponent<SpriteRenderer>().color = new Color (255, 0, 0);
	}
}
