﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class OverShield : MonoBehaviour 
{
	public GameObject agent;
	public float shieldDivideLvl;
	
	private PlayMakerFSM damageLis;
	private Vector3 overShieldScale;
	private float startOverShield;
	private float currentOverShield;
	private float overShieldDiff;
	private float startOverShieldScale;
	private float newScale;
	
	void Awake ()
	{
		PlayMakerFSM[] temp = agent.GetComponents<PlayMakerFSM>();
		foreach (PlayMakerFSM fsm in temp)
		{
			if (fsm.FsmName == "Damage Listener")
			{
				damageLis = fsm;
				break;
			}
		}
	}
	
	void Start ()
	{
		startOverShield = damageLis.FsmVariables.GetFsmInt("Shield").Value;
		
		if (startOverShield > shieldDivideLvl)
		{
			startOverShield = startOverShield / 2;
		}
		
		overShieldScale = transform.localScale;
		startOverShieldScale = overShieldScale.x;

		overShieldDiff = 1.0f;
	}
	
	void Update ()
	{
		currentOverShield = damageLis.FsmVariables.GetFsmInt ("Shield").Value;
		
		if (currentOverShield > startOverShield)
		{
			currentOverShield = currentOverShield - startOverShield;
			overShieldDiff = currentOverShield / startOverShield;
			newScale = startOverShieldScale * overShieldDiff;
			transform.localScale = new Vector3 (newScale, overShieldScale.y, overShieldScale.z);
		}
		else transform.localScale = new Vector3 (0.0f, overShieldScale.y, overShieldScale.z);
	}
	
	void LateUpdate ()
	{
		if (overShieldDiff > 0.5f)
		{
			GetComponent<SpriteRenderer>().color = new Color (200, 120, 255);
		}
		else if (overShieldDiff > 0.25)
		{
			GetComponent<SpriteRenderer>().color = new Color (255, 255, 0);
		}
		else GetComponent<SpriteRenderer>().color = new Color (255, 0, 0);
	}
}
