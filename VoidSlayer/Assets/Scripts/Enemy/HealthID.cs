﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class HealthID : MonoBehaviour 
{
	public GameObject agent;

	private PlayMakerFSM damageLis;

	// Heath Indicator
	private Vector3 healthScale;
	private float startHealth;
	private float currentHealth;
	private float healthDiff;
	private float startHealthScale;
	private float newScale;

	void Awake ()
	{
		PlayMakerFSM[] temp = agent.GetComponents<PlayMakerFSM>();
		foreach (PlayMakerFSM fsm in temp)
		{
			if (fsm.FsmName == "Damage Listener")
			{
				damageLis = fsm;
				break;
			}
		}
	}
	void Start () 
	{
		// Health constants
		startHealth = damageLis.FsmVariables.GetFsmInt("Health").Value;
		healthScale = transform.localScale;
		startHealthScale = healthScale.x;
	}

	void Update () 
	{
		// Health Indicator
		currentHealth = damageLis.FsmVariables.GetFsmInt("Health").Value;
		healthDiff = currentHealth / startHealth;
		newScale = startHealthScale * healthDiff;
		transform.localScale = new Vector3 (newScale, healthScale.y, healthScale.z);
	}

	void LateUpdate ()
	{
		// Health Colors
		if (healthDiff > 0.5f)
		{
			GetComponent<SpriteRenderer>().color = new Color (0, 255, 0);
		}
		else if (healthDiff > 0.25f)
		{
			GetComponent<SpriteRenderer>().color = new Color (255, 255, 0);
		}
		else GetComponent<SpriteRenderer>().color = new Color (255, 0, 0);
	}
}

















