﻿using UnityEngine;
using System.Collections;

public class TargetingAssist : MonoBehaviour 
{
	public GameObject targetAssist;

	// Agent
	private Vector3 agentPos;
	private float agentSpeed;

	// Player
	private GameObject player;
	private Vector3 playerPos;
	private float playerSpeed;

	// Calculations
	private float combinedSpeed;
	private float speedOffset;
	private float facingOffset;
	private float playerAgentDist;
	private float lookAngle;
	private Vector2 playerDir;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		playerSpeed = 10.0f;
	}
	void LateUpdate ()
	{
		// Get the agents position and speed
		agentPos = transform.position;
		agentSpeed = GetComponent<PolyNavAgent>().currentSpeed;

		// Get the player's position
		playerPos = player.transform.position;

		// Calculate speedOffset
		SpeedOffset();
		
		// Calculate facingOffset
		FacingOffset();

		// Apply the calculated position
		targetAssist.transform.position = new Vector3 (	agentPos.x + ((transform.up.x * agentSpeed) * facingOffset),
		                            					agentPos.y + ((transform.up.y * agentSpeed) * facingOffset),
		                            					0);

	}

	void SpeedOffset ()
	{
		// Distance between the agent and the player
		playerAgentDist = Vector2.Distance (agentPos, playerPos);
		
		// Speed of the agent and the player combined
		combinedSpeed = agentSpeed + playerSpeed;

		// Calculate speed offset
		speedOffset = playerAgentDist / combinedSpeed;
	}

	void FacingOffset ()
	{
		// Directional vector to the player
		playerDir = agentPos - playerPos;

		// Angle from the forward vector to the direction of the player
		lookAngle = Vector2.Angle (transform.up, playerDir);
		lookAngle = Mathf.Abs (lookAngle - 180);

		// Who much in front or behind as a percentage
		if (lookAngle <= 90)
		{
			facingOffset = lookAngle / 90.0f;
		}
		else 
		{
			lookAngle = Mathf.Abs (lookAngle - 180);
			facingOffset = lookAngle / 90.0f;
		}
	}
}











