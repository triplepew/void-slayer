﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using HutongGames.PlayMaker;

public class GameControl : MonoBehaviour 
{
	public static GameControl control;

	[HideInInspector] public bool isPaused = false;
	[HideInInspector] public bool physicsOn = true;

	private int currentLevel;

	//Data Elements to save
	//public GameObject Player;
	//public int LastLevelCompleted;

	//Auto load Data
	void OnEnable ()
	{
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close ();
			
			//Load data from container to local variables
			//Player = data.player;
			physicsOn = data.physics;
		}
	}
	
	//Auto save Data
	void OnDisable ()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");
		
		PlayerData data = new PlayerData();
		//Laod data into Container
		//data.player = Player;
		data.physics = physicsOn;
		
		bf.Serialize(file, data);
		file.Close();
	}

/// <summary>
/// Perform major game operations
/// --make sure there is only one <GameControl> object
/// --make sure the proper canvas is active
/// --make sure the game isn't paused
/// </summary>

	void Awake ()
	{
		if (control == null)
		{
			DontDestroyOnLoad(gameObject);
			control = this;
		}
		else if (control != this)
		{
			Destroy (gameObject);
		}

		//Ignore Enemy Weapons w/ Enemy
		Physics2D.IgnoreLayerCollision ( 14, 9, true );
		//Ignore Enemy Weapons w/ Front views
		Physics2D.IgnoreLayerCollision ( 14, 31, true );
		//Ignore Enemy Weapons w/ Enemy Weapons
		Physics2D.IgnoreLayerCollision ( 14, 14, true );
		Physics2D.IgnoreLayerCollision ( 14, 12, true );
		//Ignore Player Weapons w/ Player
		Physics2D.IgnoreLayerCollision ( 15, 8, true );
		//Ignore Player Weapons w/ Front Views
		Physics2D.IgnoreLayerCollision ( 15, 31, true );
		//Ignore Player Weapons w/ Enemy Weapons
		Physics2D.IgnoreLayerCollision ( 15, 14, true );
		Physics2D.IgnoreLayerCollision ( 15, 12, true );
	}
	
	void Update()
	{
		isPaused = FsmVariables.GlobalVariables.GetFsmBool("isPaused").Value;
		// pauses and unpauses the game
		if (isPaused) Time.timeScale = 0;
		if (!isPaused) Time.timeScale = 1;
	}

	void OnLevelWasLoaded ()
	{

	}
}

[Serializable]
class PlayerData
{
	public bool physics;
}
