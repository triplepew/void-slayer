﻿using UnityEngine;
using System.Collections;


public class EnemyMissileBH : MonoBehaviour
{
	private GameObject player;
	private Vector2 playerPos;
	private Vector2 currentPos;
	private Vector2 desiredVel;
	private Vector2 currentVel;
	private Vector2 steerVel;
	private int timeCounter;

	public float maxSpeed = 35.0f;
	public float arcSharpness = 0.45f;
	public int targetingTimer = 15;

	void OnEnable()
	{
		desiredVel = Vector2.zero;
		currentVel = Vector2.zero;
		steerVel = Vector2.zero;

		timeCounter = 0;
	}

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void Update()
	{
		//Get current velocity of Agent
		currentVel = GetComponent<Rigidbody2D>().velocity;

		//if Agent is not moving
		if (currentVel == Vector2.zero)
		{
			//Add some force
			GetComponent<Rigidbody2D>().AddForce(GetComponent<Rigidbody2D>().transform.right * maxSpeed, ForceMode2D.Force);
		}
		else
		{
			//Limit velocity of agent
			GetComponent<Rigidbody2D>().velocity = Vector2.ClampMagnitude (currentVel, maxSpeed);
		}

		timeCounter++;

		if (timeCounter >= targetingTimer)
		{
			TrackTarget ();
		}

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		AudioSource.PlayClipAtPoint(GetComponent<AudioSource>().clip, gameObject.transform.position);
		Destroy (gameObject);
	}

	private void TrackTarget()
	{
		//Get the positions of the Agent and the target
		playerPos = new Vector2 (player.transform.position.x,
		                         player.transform.position.y);
		currentPos = new Vector2 (transform.position.x,
		                          transform.position.y);
		
		//Calculate desired velocity vector
		desiredVel = playerPos - currentPos;
		desiredVel.Normalize();
		desiredVel = desiredVel * maxSpeed;
		
		//Use desired velocity to calculate steering force
		steerVel = desiredVel - currentVel;
		steerVel = steerVel * arcSharpness;  //should create larger arc
		
		//Apply steering force to agent
		GetComponent<Rigidbody2D>().AddForce (steerVel, ForceMode2D.Force);
		
		//Set rotation of agent
		float angle = Mathf.Atan2 (currentVel.x, currentVel.y) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.back);
	}
}






