﻿using UnityEngine;
using System.Collections;

public class ProjectileBehaviour : MonoBehaviour 
{
	public float lifeSpan = 3.0f;
	private float maxSpeed = 1.0f;
	private Vector2 vel;

	void OnEnable()
	{
		GetComponent<Rigidbody2D>().AddForce(GetComponent<Rigidbody2D>().transform.right, ForceMode2D.Force);
	}
	
	void Update () 
	{
		vel = GetComponent<Rigidbody2D>().velocity;

		transform.position = new Vector3 (transform.position.x + (vel.x / maxSpeed),
		                                  transform.position.y + (vel.y / maxSpeed),
		                                  0.0f);
		Destroy (gameObject, lifeSpan);
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		Destroy (gameObject);
	}
}
