﻿/* Written by Kaz Crowe */
/* SimpleJoystickEditor.cs ver. 1.1.4 */
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

[ CustomEditor( typeof( SimpleJoystick ) ) ]
public class SimpleJoystickEditor : Editor
{
	CanvasScaler canvasRect;

	/* Assigned Variables */
	SerializedProperty joystick;
	SerializedProperty joystickSizeFolder;

	/* Size and Placement */
	SerializedProperty anchor;
	SerializedProperty joystickTouchSize;
	SerializedProperty joystickSize;
	SerializedProperty radiusModifier;
	SerializedProperty tbp_X;
	SerializedProperty tbp_Y;
	SerializedProperty cs_X;
	SerializedProperty cs_Y;

	void OnEnable ()
	{
		// Here we should Initialize all of our needed variables so that we don't have to find them each updated frame
		joystick = serializedObject.FindProperty( "joystick" );
		joystickSizeFolder = serializedObject.FindProperty( "joystickSizeFolder" );
		
		/* Store our Size And Placement Variables */
		anchor = serializedObject.FindProperty( "anchor" );
		joystickTouchSize = serializedObject.FindProperty( "joystickTouchSize" );
		joystickSize = serializedObject.FindProperty( "joystickSize" );
		radiusModifier = serializedObject.FindProperty( "radiusModifier" );
		tbp_X = serializedObject.FindProperty( "tbp_X" );
		tbp_Y = serializedObject.FindProperty( "tbp_Y" );
		cs_X = serializedObject.FindProperty( "cs_X" );
		cs_Y = serializedObject.FindProperty( "cs_Y" );

		// When we have selected this object, we want to constantly update our positioning
		SimpleJoystick sj = ( SimpleJoystick )target;
		sj.needToUpdatePositioning = true;
	}

	void OnDisable ()
	{
		// When we have deselected the Simple Joystick, stop updating
		SimpleJoystick sj = ( SimpleJoystick )target;
		sj.needToUpdatePositioning = false;
	}

	/*
	For more information on the OnInspectorGUI and adding your own variables
	in the SimpleJoystick.cs script and displaying them in this script,
	see the EditorGUILayout section in the Unity Documentation to help out.
	*/

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		// Store the joystick that we are selecting
		SimpleJoystick sj = ( SimpleJoystick )target;

		/* ---------------------------------------- > ERROR CHECKING < ---------------------------------------- */
		if( canvasRect == null )
		{
			if( sj.transform.root.GetComponent<CanvasScaler>() )
				canvasRect = sj.transform.root.GetComponent<CanvasScaler>();
			else
				canvasRect = GetParentCanvas( sj );
		}
		if( canvasRect != null && canvasRect.uiScaleMode != CanvasScaler.ScaleMode.ConstantPixelSize )
		{
			EditorGUILayout.BeginVertical( "Button" );
			EditorGUILayout.HelpBox( "The Simple Joystick cannot be used correctly because the root canvas is using " + canvasRect.uiScaleMode.ToString() + ". Please place the Simple Joystick into a different Canvas with the ConstantPixelSize option.", MessageType.Error );
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if( GUILayout.Button( "Adjust Canvas", GUILayout.Width( 100 ), GUILayout.Height( 20 ) ) )
			{
				Debug.Log( "CanvasScaler / ScaleMode option has been adjusted." );
				canvasRect.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
			}
			GUILayout.FlexibleSpace();
			if( GUILayout.Button( "Adjust Joystick", GUILayout.Width( 100 ), GUILayout.Height( 20 ) ) )
			{
				bool canvasExists = false;
				Transform targetCanvas = sj.transform;
				CanvasScaler[] allCanvas = GameObject.FindObjectsOfType<CanvasScaler>();
				foreach( CanvasScaler currCanvas in allCanvas )
				{
					if( canvasExists == false )
					{
						if( currCanvas.uiScaleMode == CanvasScaler.ScaleMode.ConstantPixelSize )
						{
							canvasExists = true;
							targetCanvas = currCanvas.transform;
						}
					}
				}
				// If we didn't find a Canvas with the right options, then we need to make one
				if( canvasExists == false )
				{
					// For full comments, please refer to CreateJoystickEditor.cs
					Debug.Log( "Ultimate UI Canvas has been created." );
					GameObject root = new GameObject( "Ultimate UI Canvas" );
					root.layer = LayerMask.NameToLayer( "UI" );
					Canvas canvas = root.AddComponent<Canvas>();
					canvas.renderMode = RenderMode.ScreenSpaceOverlay;
					root.AddComponent<CanvasScaler>();
					root.AddComponent<GraphicRaycaster>();
					Undo.RegisterCreatedObjectUndo( root, "Create " + root.name );
					targetCanvas = root.transform;
				}
				
				// Here we set the joystick to be a child of the canvas
				sj.transform.SetParent( targetCanvas.transform, false );
				
				// Focus on the moved Simple Joystick gameObject
				Selection.activeGameObject = sj.gameObject;
				
				// Tell the user
				Debug.Log( "Simple Joystick has been relocated to Ultimate UI Canvas." );
				
				canvasRect = null;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
			return;
		}
		/* ---------------------------------------- > END ERROR CHECKING < ---------------------------------------- */

		EditorGUILayout.Space();

		/* ---------------------------------------- > ASSIGNED VARIABLES < ---------------------------------------- */
		EditorGUILayout.BeginVertical( "Toolbar" );
		EditorGUILayout.LabelField( "Assigned Variables", EditorStyles.boldLabel );
		EditorGUILayout.EndVertical();

		EditorGUILayout.Space();
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField( joystick );
		EditorGUILayout.PropertyField( joystickSizeFolder );
		if( EditorGUI.EndChangeCheck() )
			serializedObject.ApplyModifiedProperties();
		/* ---------------------------------------- > END ASSIGNED VARIABLES < ---------------------------------------- */

		EditorGUILayout.Space();

		/* ---------------------------------------- > SIZE AND PLACEMENT < ---------------------------------------- */
		EditorGUILayout.BeginVertical( "Toolbar" );
		EditorGUILayout.LabelField( "Size and Placement", EditorStyles.boldLabel );
		EditorGUILayout.EndVertical();

		EditorGUILayout.Space();
		// Arrange our button variables to be shown the way we want
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField( anchor, new GUIContent( "Anchor", "The side of the screen that the joystick will be anchored to" ) );
		EditorGUILayout.PropertyField( joystickTouchSize, new GUIContent( "Touch Size", "The size of the area in which the touch can be initiated" ) );
		EditorGUILayout.Slider( joystickSize, 1.0f, 4.0f, new GUIContent( "Joystick Size", "The overall size of the joystick" ) );
		EditorGUILayout.Slider( radiusModifier, 2.0f, 7.0f, new GUIContent( "Radius", "Determines how far the joystick can move visually from the center" ) );
		if( EditorGUI.EndChangeCheck() )
			serializedObject.ApplyModifiedProperties();

		EditorGUI.BeginChangeCheck();
		sj.touchBasedPositioning = EditorGUILayout.ToggleLeft( new GUIContent( "Touch Based Positioning", "Moves the joystick to the position of the initial touch" ), sj.touchBasedPositioning );
		if( EditorGUI.EndChangeCheck() )
			EditorUtility.SetDirty( target );
		
		if( sj.touchBasedPositioning == true )
		{
			EditorGUI.indentLevel = 1;
			EditorGUI.BeginChangeCheck();
			sj.overrideTouchSize = EditorGUILayout.Toggle( new GUIContent( "Override Size", "Allows a large section of the screen to be used for input" ), sj.overrideTouchSize, EditorStyles.radioButton );
			if( EditorGUI.EndChangeCheck() )
				EditorUtility.SetDirty( target );

			if( sj.overrideTouchSize == true )
			{
				EditorGUI.indentLevel = 2;
				EditorGUI.BeginChangeCheck();
				EditorGUILayout.Slider( tbp_X, 0.0f, 100.0f, new GUIContent( "Width", "The width of the Joystick Touch Area" ) );
				EditorGUILayout.Slider( tbp_Y, 0.0f, 100.0f, new GUIContent( "Height", "The height of the Joystick Touch Area" ) );
				if( EditorGUI.EndChangeCheck() )
					serializedObject.ApplyModifiedProperties();

				EditorGUI.indentLevel = 0;
			}
			EditorGUILayout.Space();
		}
		else
		{
			if( sj.overrideTouchSize == true )
				sj.overrideTouchSize = false;
		}
		EditorGUILayout.BeginVertical( "Box" );
		EditorGUILayout.LabelField( new GUIContent( "Joystick Positioning", "Customize the position of the joystick" ) );
		EditorGUI.indentLevel = 1;
		EditorGUILayout.Space();
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.Slider( cs_X, 0.0f, 50.0f, new GUIContent( "X Position:" ) );
		EditorGUILayout.Slider( cs_Y, 0.0f, 100.0f, new GUIContent( "Y Position:" ) );
		if( EditorGUI.EndChangeCheck() )
			serializedObject.ApplyModifiedProperties();

		EditorGUILayout.Space();
		EditorGUI.indentLevel = 0;
		EditorGUILayout.EndVertical();
		/* ---------------------------------------- > END SIZE AND PLACEMENT < ---------------------------------------- */

		// This is for showing helpful tips to help avoid errors
		if( sj.joystickSizeFolder == null )
			EditorGUILayout.HelpBox( "Joystick Size Folder needs to be assigned in 'Assigned Variables'!", MessageType.Error );
		if( sj.joystick == null )
			EditorGUILayout.HelpBox( "Joystick needs to be assigned in 'Assigned Variables'!", MessageType.Error );
	}

	CanvasScaler GetParentCanvas ( SimpleJoystick sj )
	{
		Transform parent = sj.transform.parent;
		while( parent != null )
		{ 
			if( parent.transform.GetComponent<CanvasScaler>() )
				return parent.transform.GetComponent<CanvasScaler>();
			
			parent = parent.transform.parent;
		}
		return null;
	}
}