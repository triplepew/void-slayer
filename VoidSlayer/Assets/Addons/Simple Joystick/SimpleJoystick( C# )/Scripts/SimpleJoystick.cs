﻿
/* Written by Kaz Crowe */
/* SimpleJoystick.cs ver. 1.1.4 */
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/* 
 * First off, we are using [ExecuteInEditMode] to be able to show changes in real time.
 * This will not affect anything within a build or play mode. This simply makes the script
 * able to be run while in the Editor in Edit Mode.
*/
[ExecuteInEditMode]
[System.Serializable]
public class SimpleJoystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
	/* Basic Variables */
	public RectTransform joystick;
	public RectTransform joystickSizeFolder;
	Vector3 joystickCenter;
	Vector2 textureCenter;
	Vector2 defaultPos;
	/* Size and Placement */
	public enum Anchor
	{
		Left,
		Right
	}
	public Anchor anchor;
	public enum JoystickTouchSize
	{
		Default,
		Medium,
		Large
	}
	public JoystickTouchSize joystickTouchSize;
	public float joystickSize = 2.5f;
	public float radiusModifier = 4.5f;
	float radius;
	public bool touchBasedPositioning;
	public bool overrideTouchSize;
	public float tbp_X = 50.0f;
	public float tbp_Y = 75.0f;
	public float cs_X = 5.0f;
	public float cs_Y = 20.0f;

	
	void Start ()
	{
		// First off, UpdatePositioning of the joystick
		if( Application.isPlaying == true )
			UpdatePositioning();
	}
	
	// This function allows other scripts to get our joysticks position data
	public Vector2 JoystickPosition
	{
		get
		{
			// Make a temporary Vector2 and divide it by our radius to give us a value between -1 and 1
			Vector2 tempVec = joystick.position - joystickCenter;
			return tempVec / radius;
		}
	}

	// This function gets us the distance of the joystick from center
	public float JoystickDistance
	{
		get
		{
			float joystickDistance = Vector3.Distance( joystick.position, joystickCenter ) / radius;
			return joystickDistance;
		}
	}

	// This means we have touched, so process where we have touched
	public void OnPointerDown ( PointerEventData touchInfo )
	{
		// If we are using the Touch Based Positioning we need to do a few things here
		if( touchBasedPositioning == true )
		{
			// We need to move our joysticSizeFolder to the point of our touch
			joystickSizeFolder.position = touchInfo.position - textureCenter;

			// Set our center to the touch position since it has changed
			joystickCenter = touchInfo.position;
		}

		// Call UpdateJoystick with the info from our PointerEventData
		UpdateJoystick( touchInfo );
	}

	// This means we are moving
	public void OnDrag ( PointerEventData touchInfo )
	{
		// We need to call our UpdateJoystick and process where we are touching
		UpdateJoystick( touchInfo );
	}

	// This means we have let go
	public void OnPointerUp ( PointerEventData touchInfo )
	{
		// If we are using Touch Based Positioning, then we need to change a few things
		if( touchBasedPositioning == true )
		{
			// we need to set our folder back to defaultPos
			joystickSizeFolder.position = defaultPos;

			// set our joystickCenter again since it changed when we touched down
			joystickCenter = ( Vector2 )joystickSizeFolder.position + textureCenter;
		}

		// Set the joystick to our center
		joystick.position = joystickCenter;
	}

	// This function updates our joystick according to our touch
	void UpdateJoystick ( PointerEventData touchInfo )
	{
		// Create a new Vector2 to equal the vector from our curret touch to the center of joystick
		Vector2 tempVector = touchInfo.position - ( Vector2 )joystickCenter;

		// Clamp the Vector, which will give us a round boundary
		tempVector = Vector2.ClampMagnitude( tempVector, radius );

		// Apply the Vector to our Joystick's position
		joystick.transform.position = ( Vector2 )joystickCenter + tempVector;
	}

	// This function updates our options. It is public so we can call it from other scripts to update our positioning
	public void UpdatePositioning ()
	{
		// We want our joystick size to be larger when we have a larger number, so we need to calculate that out
		float textureSize = Screen.height * ( joystickSize / 10 );

		// Same with our radius modifier
		radius = textureSize * ( radiusModifier / 10 );

		// We need to store this object's RectTrans so that we can position it
		RectTransform baseTrans = GetComponent<RectTransform>();

		// Check and make sure our joystick has no rotation or scale to it
		if( baseTrans.transform.localScale != Vector3.one )
			baseTrans.transform.localScale = Vector3.one;
		if( baseTrans.rotation != Quaternion.identity )
			baseTrans.rotation = Quaternion.identity;

		// We need to get a position for our joystick based on our position variable
		Vector2 joystickTexturePosition = ConfigureTexturePosition( textureSize );

		// If we are wanting our touch size to override our JoystickTouchSize option
		if( overrideTouchSize == true )
		{
			// Fix our size variables
			float fixedFBPX = tbp_X / 100;
			float fixedFBPY = tbp_Y / 100;
			
			// Depending on our joystickTouchSize options, we need to configure our size
			baseTrans.sizeDelta = new Vector2( Screen.width * fixedFBPX, Screen.height * fixedFBPY );
			
			// If anchor is set to Left, then we want to set it in the bottom left corner
			if( anchor == Anchor.Left )
				baseTrans.position = new Vector2( 0, 0 );
			else // it is right, then we want it to be in the center of our screen
			{
				if( overrideTouchSize == true )
					baseTrans.position = new Vector2( Screen.width - baseTrans.sizeDelta.x, 0 );
				else
					baseTrans.position = new Vector2( Screen.width / 2, 0 );
			}
		}
		else
		{
			// Our touch size needs to be fixed to a float value
			float fixedTouchSize;
			if( joystickTouchSize == JoystickTouchSize.Large )
				fixedTouchSize = 2.0f;
			else if( joystickTouchSize == JoystickTouchSize.Medium )
				fixedTouchSize = 1.51f;
			else
				fixedTouchSize = 1.01f;

			// Make a temporary Vector2
			Vector2 tempVector = new Vector2( textureSize, textureSize );

			// Our touch area is standard, so set it up with our tempVector multiplied by our fixedTouchSize
			baseTrans.sizeDelta = tempVector * fixedTouchSize;

			// We get our texture position and modify it with our sizeDelta - tempVector ( gives us the difference ) and divide by 2
			baseTrans.position = joystickTexturePosition - ( ( baseTrans.sizeDelta - tempVector ) / 2 );
		}

		if( touchBasedPositioning == true )
		{
			// We need to know our texture's center so that we can move it to our touch position correctly
			textureCenter = new Vector2( textureSize / 2, textureSize / 2 );
			
			// Also need to store our default position so that we can return after the touch has been lifted
			defaultPos = joystickTexturePosition;
		}

		// Our joystickSizeFolder needs to be textureSize and texture position
		joystickSizeFolder.sizeDelta = new Vector2( textureSize, textureSize );
		joystickSizeFolder.position = joystickTexturePosition;

		// Store the joystick center so we can return to it
		joystickCenter = joystick.position;
	}

	// This function will configure a Vector2 for the position of our Joystick
	Vector2 ConfigureTexturePosition ( float textureSize )
	{
		// We need a few temporary Vector2's to work with
		Vector2 tempPosVector;

		// we need to fix our custom spacing variables to be usable
		float fixedCSX = cs_X / 100;
		float fixedCSY = cs_Y / 100;
		
		// We also need two floats for applying our spacers according to our screen size
		float positionSpacerX = Screen.width * fixedCSX - ( textureSize * fixedCSX );
		float positionSpacerY = Screen.height * fixedCSY - ( textureSize * fixedCSY );

		// If it's left, we can simply apply our positionxSpacerX
		if( anchor == Anchor.Left )
			tempPosVector.x = positionSpacerX;
		// else it's to the right, we need to calculate out from our right side and apply our positionSpaceX
		else
			tempPosVector.x = ( Screen.width - textureSize ) - positionSpacerX;

		// Here we just apply our positionSpacerY
		tempPosVector.y = positionSpacerY;
		return tempPosVector;
	}

	/* 
	 * This function allows us to apply changes in screen size and joystick options in real time
	 * However anything within this #if section will not be run in a game build, only within Unity
	*/
#if UNITY_EDITOR
	Vector3 lastScreenSize;
	public bool needToUpdatePositioning = false;
	
	void Update ()
	{
		// We want to constantly keep our joystick updated while the game is not being run in Unity
		if( Application.isPlaying == false )
		{
			// But only if we have values that have changed
			if( needToUpdatePositioning == true )
				UpdatePositioning();
		}
		
		Vector3 currentScreenSize = new Vector2( Screen.width, Screen.height );
		// Or if our screen size has changed
		if( lastScreenSize != currentScreenSize )
		{
			lastScreenSize = currentScreenSize;
			UpdatePositioning();
		}
	}
#endif
}