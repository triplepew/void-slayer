﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Set destination for a PolyNav Agent to a target gameObject position")]
	public class MoveToGameObject : FsmStateAction {

		[RequiredField]
		[CheckForComponent(typeof(PolyNavAgent))]
		public FsmOwnerDefault agent;
		public FsmGameObject targetGameObject;
		public FsmEvent invalidEvent;
		public FsmEvent reachEvent;
		public bool everyFrame;

		private PolyNavAgent navAgent;

		public override void Reset(){
			agent = null;
			targetGameObject = null;
		}

		public override void OnEnter(){

			var go = Fsm.GetOwnerDefaultTarget(agent);
			if (go == null){
				Finish();
				return;
			}

			navAgent = go.GetComponent<PolyNavAgent>();

			Do();
			if (!everyFrame)
				Finish();
		}

		public override void OnUpdate(){
			Do();
		}

		void Do(){
			
			navAgent.SetDestination(targetGameObject.Value.transform.position, Callback);
		}

		void Callback(bool isValid){

			if (isValid)
				Fsm.Event(reachEvent);

			if (!isValid)
				Fsm.Event(invalidEvent);

			if (!everyFrame){
				Finish();
			}
		}

		public override void OnExit(){

			navAgent.Stop();
		}
	}
}