﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Stops a PolyNavAgent from moving and resets it's path")]
	public class Stop : FsmStateAction {

		[RequiredField]
		[CheckForComponent(typeof(PolyNavAgent))]
		public FsmOwnerDefault agent;

		public override void Reset(){
			agent = null;
		}

		public override void OnEnter(){

			var go = Fsm.GetOwnerDefaultTarget(agent);
			if (go == null){
				Finish();
				return;
			}

			go.GetComponent<PolyNavAgent>().Stop();
			Finish();
		}
	}
}