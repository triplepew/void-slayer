﻿using UnityEngine;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Checks if position is a valid PolyNav point")]
	public class IsValidPolyNavPoint : FsmStateAction {

		public FsmVector2 position;
		public FsmEvent validEvent;
		public FsmEvent invalidEvent;

		public override void Reset(){

			position = null;
		}

		public override void OnEnter(){

			if (!PolyNav2D.current){
				Finish();
				return;
			}
			
			Fsm.Event(PolyNav2D.current.PointIsValid(position.Value)? validEvent : invalidEvent);
			Finish();
		}
	}
}