﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Set destination for a PolyNav Agent to a target vector position")]
	public class MoveToPosition : FsmStateAction {

		[RequiredField]
		[CheckForComponent(typeof(PolyNavAgent))]
		public FsmOwnerDefault agent;
		public FsmVector2 targetPosition;
		public FsmEvent invalidEvent;
		public FsmEvent reachEvent;
		public bool everyFrame;

		private PolyNavAgent navAgent;

		public override void Reset(){
			agent = null;
			targetPosition = null;
		}

		public override void OnEnter(){

			var go = Fsm.GetOwnerDefaultTarget(agent);
			if (go == null){
				Finish();
				return;
			}

			navAgent = go.GetComponent<PolyNavAgent>();
			Do();
			if (!everyFrame)
				Finish();
		}

		public override void OnUpdate(){
			Do();
		}

		void Do(){
			
			navAgent.SetDestination(targetPosition.Value, Callback);
		}

		void Callback(bool isValid){

			if (isValid)
				Fsm.Event(reachEvent);

			if (!isValid)
				Fsm.Event(invalidEvent);

			if (!everyFrame){
				Finish();
			}
		}

		public override void OnExit(){

			navAgent.Stop();
		}
	}
}