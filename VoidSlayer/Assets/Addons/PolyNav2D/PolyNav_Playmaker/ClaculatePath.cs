﻿using UnityEngine;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Claculate a path and send event if path exists or not")]
	public class CalculatePath : FsmStateAction {

		public FsmVector2 from;
		public FsmVector2 to;
		public FsmEvent pathExistEvent;
		public FsmEvent pathNotExistEvent;

		public override void Reset(){

			from = null;
			to = null;
		}

		public override void OnEnter(){

			if (!PolyNav2D.current){
				Finish();
				return;
			}

			PolyNav2D.current.FindPath(from.Value, to.Value, PathReady);
		}

		void PathReady(Vector2[] path, bool success){
			Fsm.Event( success? pathExistEvent : pathNotExistEvent );
			Finish();
		}
	}
}