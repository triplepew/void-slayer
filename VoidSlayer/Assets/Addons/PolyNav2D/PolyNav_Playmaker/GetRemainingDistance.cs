﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Get the remaining path distance of a PolyNavAgent")]
	public class GetRemainingDistance : FsmStateAction {

		[RequiredField]
		[CheckForComponent(typeof(PolyNavAgent))]
		public FsmOwnerDefault agent;
		
		[UIHint(UIHint.Variable)]
		public FsmFloat store;
		public bool everyFrame;

		private PolyNavAgent navAgent;

		public override void Reset(){
			agent = null;
			store = null;
		}

		public override void OnEnter(){
			
			var go = Fsm.GetOwnerDefaultTarget(agent);
			if (go == null){
				Finish();
				return;
			}
			navAgent = go.GetComponent<PolyNavAgent>();
			Do();
		}

		public override void OnUpdate(){

			Do();
		}	

		void Do(){

			store.Value = navAgent.remainingDistance;
			if (!everyFrame){
				Finish();
			}
		}
	}
}