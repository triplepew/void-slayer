﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions{

	[ActionCategory("PolyNav")]
	[Tooltip("Get current destination of a PolyNavAgent")]
	public class GetCurrentDestination : FsmStateAction {

		[RequiredField]
		[CheckForComponent(typeof(PolyNavAgent))]
		public FsmOwnerDefault agent;
		
		[UIHint(UIHint.Variable)]
		public FsmVector2 store;
		public bool everyFrame;

		private PolyNavAgent navAgent;

		public override void Reset(){
			agent = null;
			store = null;
		}

		public override void OnEnter(){
			
			var go = Fsm.GetOwnerDefaultTarget(agent);
			if (go == null){
				Finish();
				return;
			}
			navAgent = go.GetComponent<PolyNavAgent>();
			Do();
		}

		public override void OnUpdate(){

			Do();
		}	

		void Do(){

			store.Value = navAgent.primeGoal;
			if (!everyFrame){
				Finish();
			}
		}
	}
}